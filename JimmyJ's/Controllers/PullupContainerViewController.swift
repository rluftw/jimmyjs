//
//  PullupContainerViewController.swift
//  JimmyJ's
//
//  Created by Xing Hui Lu on 8/26/18.
//  Copyright © 2018 Xing Hui Lu. All rights reserved.
//

import UIKit

class PullupContainerViewController: UIViewController {

    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension PullupContainerViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        scrollView.clipsToBounds = false
    
        let yPosition = scrollView.frame.origin.y
        let offset = velocity.y > 0 ? -20 : 20
        let newYPosition = yPosition + CGFloat(offset)
        
        let height = containerView.frame.height
        let newHeight = height + CGFloat(offset)
        
        print(yPosition)
        print(newYPosition)
        
        print("height: \(height)")
        print("new height: \(newHeight)")
        
        scrollView.frame.origin.y = newYPosition
        scrollView.contentSize.height = newHeight
    }
}
